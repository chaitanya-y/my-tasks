import { handleActions } from 'modules/helpers.js';
import {  ActionTypes } from 'constants/index';

 const taksDataState = {
   tasksData:{}

};

//reducer for get tasks success
export default {
  
  tasksData: handleActions(
    
    {
      [ActionTypes.GET_TASKS_DATA_SUCCESS]: (state,{payload}) => {
          state.tasksData = payload;
      },
    },
    taksDataState,
  ),
};



