import user from './user';
import tasksData from './tasksReducer';

export default {
  ...tasksData,
  ...user,
};
