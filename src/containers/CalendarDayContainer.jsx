import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Box, Typography } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { getDate, isSameMonth, isSameDay, format } from 'date-fns';
import '../styles/calendar.css';



class CalenderDay extends Component {


    render() {
        const { tasksData } = this.props;
        const { cellDate, calendarDate, } = this.props;

        const isToday = isSameDay(cellDate, new Date());
        const avatarClass = isToday
            ? 'today-avatar'
            : 'date-number';
        var cellDateFormat = tasksData[format(cellDate, 'dd/MM/yyyy')];


        return (
            <div
                className={isSameMonth(cellDate, calendarDate) ? 'day-cell' : 'day-cell-outside-month'}
            >
                <Avatar className={avatarClass}>{getDate(cellDate)}</Avatar>
                {/* logic for displaying pending and upcoming tasks */}
                {
                    tasksData != null && tasksData != undefined && cellDateFormat != undefined && cellDateFormat != null &&
                        cellDateFormat != {} && (cellDateFormat.pending != undefined || cellDateFormat.upcoming != undefined) ?
                        <h4 className='tasks-number'><pre className={cellDateFormat.pending != undefined ? 'pending' : 'upcoming'}>
                            {cellDateFormat.pending != undefined ? cellDateFormat.pending :
                                cellDateFormat.upcoming} {cellDateFormat.pending != undefined ?
                                    (cellDateFormat.pending == 1 ? 'Task is Due' : 'Tasks are Due') : cellDateFormat.upcoming != undefined ? (cellDateFormat.upcoming == 1 ?
                                        'upcoming Task' : 'Upcoming Tasks') : ''}
                        </pre>
                        </h4> : <div />
                }
            </div>
        );


    }


}

function mapStateToProps(state) {
    return {
        tasksData: state.tasksData.tasksData
    };
}



export default connect(
    mapStateToProps,
)(CalenderDay);

