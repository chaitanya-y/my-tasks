import React, { Component } from 'react';
import { connect } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import * as dateFns from 'date-fns';
import NavBar from '../components/NavBarComponent';
import CalendarGrid from '../components/CalendarGridComponent';
import { tasksData } from '../actions/index';
import '../styles/calendar.css';
import '../styles/navbar.css';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';




class MyTasks extends Component {
  constructor() {
    super();
    this.state = {
      date: new Date(),
      filter: 0

    };
  }

  /* 
   To decrement the state date variable by one month and to create the calendar table for previous month 
   */
  prevMonth = () => {
    this.setState({ date: dateFns.subMonths(this.state.date, 1) });
    const { getTasksData } = this.props;
    const { filter } = this.state;
    getTasksData(dateFns.subMonths(this.state.date, 1), filter == 1 ? 'pending' : filter == 2 ? 'upcoming' : '');

  };

  // To increment the state date variable by one month and to create the calendar table for next month 
  nextMonth = () => {
    this.setState({ date: dateFns.addMonths(this.state.date, 1) });
    const { getTasksData } = this.props;
    const { filter } = this.state;
    getTasksData(dateFns.addMonths(this.state.date, 1), filter == 1 ? 'pending' : filter == 2 ? 'upcoming' : '');
  };

  // To handle the tasks filter(pending, upcoming)
  handleChange = (event) => {
    this.setState({
      filter: event.target.value
    })
    const { getTasksData } = this.props;
    getTasksData(this.state.date, event.target.value == 1 ? 'pending' : event.target.value == 2 ? 'upcoming' : '');
  };


  render() {
    const { date, filter } = this.state;
    const month = date.toLocaleString('en-us', { month: 'long' });
    const year = dateFns.getYear(date);


    return (
      <div className='root'>
        <Paper className='calendar'>
          <NavBar title='My Tasks' />
          <div className='filter-container'>
            <Select
              value={filter}
              onChange={this.handleChange}
              style={{ width: '100px' }}
            >
              <MenuItem value={0} className='select-item' >Filters</MenuItem>
              <MenuItem value={1} className='select-item'>Pending</MenuItem>
              <MenuItem value={2} className='select-item'>Upcoming</MenuItem>
            </Select>

          </div>
          <header className='calendar-header'>
            <IconButton aria-label="Last Month" onClick={this.prevMonth}>
              <KeyboardArrowLeftIcon fontSize="large" />
            </IconButton>
            <Typography variant="h5">
              {month} {year}
            </Typography>
            <IconButton aria-label="Next Month" onClick={this.nextMonth}>
              <KeyboardArrowRightIcon fontSize="large" />
            </IconButton>
          </header>
          <CalendarGrid date={date} />
        </Paper>
      </div>
    );

  };
}



function mapStateToProps(state) {
  return { user: state.user };
}

function mapDispatchToProps(dispatch) {
  return {
    getTasksData: (date, filter) => {
      dispatch(tasksData(date, filter));
    }

  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyTasks);                                                                                      