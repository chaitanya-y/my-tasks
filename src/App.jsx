import { hot } from 'react-hot-loader/root';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Router, Switch, Route } from 'react-router-dom';
import history from 'modules/history';
import NotFound from 'components/NotFound';
import RoutePublic from 'components/RoutePublic';
import {tasksData} from 'actions/index';
import MyTasks from 'containers/MyTasksContainer';

export class App extends React.Component {

  static propTypes = {
    getTasksData: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
  };

 

  render() {
    const { user } = this.props;
    //  react-router and route public is component for redirecting
    return (
      <Router history={history}>
              <Switch>
                <RoutePublic
                  isAuthenticated={user.isAuthenticated}
                  path="/"
                  exact
                  component={MyTasks}
                />
                <Route component={NotFound} />
              </Switch>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    tasksData: state.tasksData,
  };
}


function mapDispatchToProps(dispatch) {
  return {
    getTasksData: (date,filter) => {
      dispatch(tasksData(date,filter));
    },
  };
}

export default hot(connect(mapStateToProps,mapDispatchToProps)(App));
