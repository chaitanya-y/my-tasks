
/* 
 createActions is helper function from redux-actions
 */
import { createActions } from 'redux-actions';
import { ActionTypes } from 'constants/index';

// tasksData is an action for fetching tasks data
export const { getTasksData: tasksData, } = createActions({
  [ActionTypes.GET_TASKS_DATA]: (date,filter) => ({date,filter}),
});

