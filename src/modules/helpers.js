import produce from 'immer';
import { format } from 'date-fns';


// Function which returns new state
export function handleActions(actionsMap, defaultState) {
  return (state = defaultState, { type, ...rest } = {}) =>
    produce(state, (draft) => {
      const action = actionsMap[type];
      let newState;

      if (action) {
        newState = action(draft, rest);
      }

      if (newState) {
        return newState;
      }

      return draft;
    });
}


// function to create action types
export function keyMirror(obj) {
  const output = {};

  for (const key in obj) {
    if (!Object.prototype.hasOwnProperty.call(output, key)) {
      output[key] = key;
    }
  }
  return output;
}

// Log grouped messages to the console
export function logger(type, title, data, options = {}) {
  if (process.env.NODE_ENV === 'development') {
    const { collapsed = true, hideTimestamp = false, typeColor = 'gray' } = options;
    const groupMethod = collapsed ? console.groupCollapsed : console.group;
    const parts = [`%c ${type}`];
    const styles = [`color: ${typeColor}; font-weight: lighter;`, 'color: inherit;'];

    if (!hideTimestamp) {
      styles.push('color: gray; font-weight: lighter;');
    }

    const time = format(new Date(), 'HH:mm:ss');

    parts.push(`%c${title}`);

    if (!hideTimestamp) {
      parts.push(`%c@ ${time}`);
    }

    if (!window.SKIP_LOGGER) {
      groupMethod(parts.join(' '), ...styles);
      console.log(data);
      console.groupEnd();
    }
  }
}

export const spread = produce(Object.assign);
