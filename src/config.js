// Configuration

const config = {
  name: 'My Tasks',
  description: 'Faculty calendar dashboard',
};

export default config;
