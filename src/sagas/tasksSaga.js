
import { all, put, takeLatest } from 'redux-saga/effects';
import generateData from '../utils/dataGenerator';
import { ActionTypes } from '../constants/index';



// Get tasks data saga async action
export function* tasksData({ payload }) {
  try {
    const tasksData = yield generateData(payload);

      yield put({
        type: ActionTypes.GET_TASKS_DATA_SUCCESS,
        payload: tasksData
      });
  } catch (err) {
    yield put({
      type: ActionTypes.GET_TASKS_DATA_FAILURE,
      payload: err,
    });
  }
}

//  Tasks Sagas
export default function* root() {
  yield all([
    takeLatest(ActionTypes.GET_TASKS_DATA, tasksData),
  ]);
}
