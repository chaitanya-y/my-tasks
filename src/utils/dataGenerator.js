import { getYear, getMonth, format, getDate } from 'date-fns';

/* Dummy data generator */
function generateData(payload, ) {
  var currentDate = new Date();
  var currentMonth = getMonth(currentDate);
  const selectedYear = getYear(payload.date);
  const selectedMonth = getMonth(payload.date);
  const dataObj = {};

  if (selectedMonth > currentMonth && payload.filter == 'pending' || (selectedMonth == currentMonth && payload.filter == 'upcoming' && getDate(currentDate) > 30) || (selectedMonth > currentMonth && payload.filter == 'upcoming' && getDate(currentDate) > 30)) {
    return dataObj;
  }
  if (currentMonth == selectedMonth || selectedMonth == currentMonth + 1) {
    for (let i = 0; i < 3; i++) {
      var pendingOrUpcomingLimit = payload.filter == 'pending' ? (getDate(currentDate) != 0 ? getDate(currentDate) - 1 : 1) : (getDate(currentDate) ? + getDate(currentDate) + 1 : 1);
      var limit30 = Math.floor(Math.random() * (30 - pendingOrUpcomingLimit)) + pendingOrUpcomingLimit;
      var limit5 = (Math.floor((Math.random() * 5) + 1));
      var key = `${limit30 >= 10 ? limit30 : '0' + (limit30)}/${selectedMonth + 1 >= 10 ? selectedMonth + 1 : '0' + (selectedMonth + 1)}/${selectedYear}`;
      var value = {};
      value[payload.filter.toLowerCase()] = limit5
      dataObj[key] = value

    }
  }



  return dataObj;

}


export default generateData;
