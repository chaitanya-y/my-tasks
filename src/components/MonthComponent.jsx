import React from 'react';
import CalenderDay from '../containers/CalendarDayContainer';



const MonthComponent = (props) => (
	<div className='month-container'>
		{props.calendarCells.map((cell, i) => (
			<CalenderDay key={i} calendarDate={props.date} cellDate={cell.date} />
		))}
	</div>
);

export default MonthComponent;

