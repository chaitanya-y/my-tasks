import Typography from '@material-ui/core/Typography';
import React from 'react';

//component which returns day name 
const DayName = (props) => <Typography variant="h5" className = 'day-name'>{props.day}</Typography>;
export default DayName;
