import React from 'react';
import { daysArr } from '../utils/dateUtils';
import DayName from './DayNameComponent';
import '../styles/calendar.css';

// component which returns days row
const DaysRow = (props) => (
	<div className='days-row'>
		{daysArr.map((day, i) => (
			<DayName key={i} day={day} />
		))}
	</div>
);

export default DaysRow;
