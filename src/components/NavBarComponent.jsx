import React, { Component } from 'react';
import { connect } from 'react-redux';
import 'styles/navbar.css';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
class NavBar extends Component {
 


  render() {
    const { title } = this.props;


    if (!title) {
      return null;
    }


    return (

      <div className='navbar-container' data-test='NavBarComponent'>
        <div className='navbar-logo-container'>

          <img
            className='logo'
            src="../media/images/logo.png"
            alt="Byju's logo"
            data-test='logo'
          />
          <div className="text-tag" data-test='title'>{title}</div>

        </div>
      </div>
    );
  }
}

NavBar.propTypes = {
  title: PropTypes.string
}

export default NavBar;
