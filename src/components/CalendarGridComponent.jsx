import React from 'react';
import { getMonthCells } from '../utils/dateUtils';
import DaysRow from './DaysRowComponent';
import MonthComponent from './MonthComponent';
import '../styles/calendar.css';

// caleandar grid main container which has days row and month calendar
const CalendarGrid = (props) => {
	const { date } = props;
	const calendarCells = getMonthCells(date);
	return (
		<div className='calendar-grid'>
			<DaysRow />
			<MonthComponent date={date} calendarCells={calendarCells} />
		</div>
	);
};

export default CalendarGrid;
