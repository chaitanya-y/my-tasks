import { ActionTypes } from '../../src/constants';
import TasksReducer from '../../src/reducers/tasksReducer';

describe('Tasks Reducer', () => {

    it('Should return default state', () => {
        const newState = TasksReducer.tasksData(undefined, {});
        expect(newState).toEqual({
            tasksData: {}
        });
    });

    it('Should return new state if receiving type', () => {

        const tasksDataState = {
            tasksData: {
                '29/06/2020': {
                    'pending': 1
                }
            }
        }
        const newState = TasksReducer.tasksData(undefined, {
            type: ActionTypes.GET_TASKS_DATA_SUCCESS,
            payload: tasksDataState.tasksData
        });
        expect(newState).toEqual(tasksDataState);

    });

});