import React from 'react';
import { shallow } from 'enzyme';
import NavBar from '../../src/components/NavBarComponent';
import { findByTestAtrr, checkProps } from '../testUtils'

const setUp = (props = {}) => {
    const component = shallow(<NavBar {...props} />);
    return component;
};

describe('NavBar Component', () => {

    describe('Checking PropTypes', () => {

        it('Should not throw a warning', () => {

            const expectedProps = {
                title: 'My Tasks',

            };
            const propsErr = checkProps(NavBar, expectedProps)
            expect(propsErr).toBeUndefined();


        });

    });

    describe('Have props', () => {

        let wrapper;
        beforeEach(() => {
            const props = {
                title: 'My Tasks',
            };
            wrapper = setUp(props);
        });

        it('Should render without errors', () => {
            const component = findByTestAtrr(wrapper, 'NavBarComponent');
            expect(component.length).toBe(1);
        });

        it('Should render a logo', () => {
            const h1 = findByTestAtrr(wrapper, 'logo');
            expect(h1.length).toBe(1);
        });

        it('Should render a title', () => {
            const title = findByTestAtrr(wrapper, 'title');
            expect(title.length).toBe(1);
        });

    });

    describe('Have NO props', () => {

        let wrapper;
        beforeEach(() => {
            wrapper = setUp();
        });

        it('Should not render', () => {
            const component = findByTestAtrr(wrapper, 'NavBarComponent');
            expect(component.length).toBe(0);
        });


    });


});