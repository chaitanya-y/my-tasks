import moxios from 'moxios';
import { testStore } from '../testUtils';
import {tasksData} from '../../src/actions';
describe('Get tasks Data action', () => {

    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    test('Store is updated correctly', () => {
    /*
    Expected state is tasks data length not a object Because data 
    generator will generate different data each time. It is not possible to compare.
    */
        const expectedState = 10;
        const store = testStore;

        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: 10
            })
        });

        return store.dispatch(tasksData(Date(), 'pending'))
        .then(() => {
            const newState = store.getState();
            console.log(newState)
            expect(expectedState).toBe(10);
        })
        
    });

});