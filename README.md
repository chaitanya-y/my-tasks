### About the project :
Challenge is to develop a calendar which could display all the pending and upcoming tasks of a particular faculty/member.


### Architecture :
Architecture & Design Principle: Followed the React + Redux architecture which would let us simplify in managing the state of the system irrespective of it’s size and can achieve scalability. Have also built using Saga, so that all the actions are asynchronous and thereby letting concurrent calls happen at a time.
Dashboards are customary in any assesment dashboards which have considerably more data and features in which it will be difficult to handle state at component level. Redux provides global state which can be connected to multiple components.


### Dependencies :
  - react ^16.x
  - react-router 4.x
  - redux 4.x
  - redux-saga 0.16.x

### Installation :
  - git clone the repo => https://gitlab.com/chaitanya-y/my-tasks.git
  - go to root directory
  - `npm install ` which installs all the required dependencies (refer: package.json)
  

### Development :
  - webpack-dev-server 3.x
  - react-hot-loader 4.x

  - `npm start`


### Building :
  - webpack 4.x
  - babel 7.x

  - `npm run build`


### Testing :
  - jest 23.x
  - enzyme 3.x
  - `npm test`


### Time taken :
   - Frontend : 1 day
   - TestCases & Debugging: 3 hours(Unit Testing & Integration Testing, tried and tested)

### Learnings:
  - Integration Testing

### Future Enhancements:
  - Filtering the number of tasks pending or upcoming based on subjects
  - Adding reminders for particular dates


### Pendings :
  - Left Dock on the page.
  - User profile
